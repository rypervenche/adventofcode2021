const INPUT: &str = include_str!("../resources/input.txt");

fn find_total(input: &str) -> usize {
    input
        .split("\n\n")
        .map(|elf| {
            elf.trim()
                .split('\n')
                .map(|num| num.trim().parse::<usize>().expect("Failed to parse digit"))
                .sum()
        })
        .max()
        .expect("Failed to find maximum.")
}

fn main() {
    let max = find_total(INPUT);
    println!("{max}");
}

#[cfg(test)]
mod tests {
    use super::*;
    const TEST_INPUT: &str = include_str!("../resources/test_input.txt");

    #[test]
    fn output() {
        assert_eq!(24000, find_total(TEST_INPUT));
    }
}
