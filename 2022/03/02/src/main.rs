use std::collections::HashSet;

const INPUT: &str = include_str!("../resources/input.txt");

fn find_item_value(vec: &[&str]) -> usize {
    let set: HashSet<char> = vec
        .iter()
        .map(|line| HashSet::from_iter(line.chars()))
        .collect::<Vec<HashSet<char>>>()
        .into_iter()
        .reduce(|acc, e| acc.intersection(&e).copied().collect::<HashSet<char>>())
        .unwrap();

    let char = set.iter().next().unwrap();
    let num = match char {
        'a'..='z' => *char as usize - 96,
        'A'..='Z' => *char as usize - 38,
        _ => panic!("Bad char."),
    };
    num
}

fn get_total(string: &str) -> usize {
    string
        .trim()
        .split('\n')
        .collect::<Vec<&str>>()
        .chunks(3)
        .map(|c| find_item_value(c))
        .sum()
}

fn main() {
    let total = get_total(INPUT);
    println!("{total}");
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../resources/test_input.txt");
    #[test]
    fn output() {
        assert_eq!(70, get_total(TEST_INPUT));
    }
}
