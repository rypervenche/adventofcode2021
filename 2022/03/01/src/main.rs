use std::collections::HashSet;

const INPUT: &str = include_str!("../resources/input.txt");

fn find_dups(string: &str) -> usize {
    let lines = string.trim().split('\n').collect::<Vec<&str>>();
    let mut nums = Vec::new();
    for line in lines {
        let half = line.len() / 2;
        let split = line.split_at(half);
        let set1: HashSet<char> = HashSet::from_iter(split.0.chars());
        let set2: HashSet<char> = HashSet::from_iter(split.1.chars());
        let mut both = set1.intersection(&set2);
        let char = both.next().expect("No shared character.");
        let num = match char {
            'a'..='z' => *char as usize - 96,
            'A'..='Z' => *char as usize - 38,
            _ => panic!("Bad char."),
        };
        nums.push(num);
    }
    nums.iter().sum()
}

fn main() {
    let total = find_dups(INPUT);
    println!("{total}");
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../resources/test_input.txt");
    #[test]
    fn output() {
        assert_eq!(157, find_dups(TEST_INPUT));
    }
}
