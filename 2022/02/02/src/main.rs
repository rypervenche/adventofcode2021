use strum::EnumString;

const INPUT: &str = include_str!("../resources/input.txt");

#[derive(EnumString, Copy, Clone, Debug)]
enum HisChoice {
    A = 1,
    B = 2,
    C = 3,
}

#[derive(EnumString, Copy, Clone, Debug)]
enum MyChoice {
    X = 1,
    Y = 2,
    Z = 3,
}

enum GameResult {
    Win = 6,
    Lose = 0,
    Draw = 3,
}

impl MyChoice {
    fn play(&self, his: &HisChoice) -> usize {
        match self {
            MyChoice::X => {
                let mychoice = match *his as usize - 1 {
                    0 => 3,
                    n => n,
                };
                mychoice + GameResult::Lose as usize
            }
            MyChoice::Y => *his as usize + GameResult::Draw as usize,
            MyChoice::Z => {
                let mychoice = match *his as usize + 1 {
                    4 => 1,
                    n => n,
                };
                mychoice + GameResult::Win as usize
            }
        }
    }
}

fn full_game(input: &str) -> usize {
    input
        .trim()
        .split('\n')
        .map(|l| {
            let vec = l.split_ascii_whitespace().collect::<Vec<&str>>();
            let c1 = vec[0];
            let c2 = vec[1];
            let his = c1
                .parse::<HisChoice>()
                .expect("Failed to create his Choice.");
            let my = c2.parse::<MyChoice>().expect("Failed to create MyChoice.");
            my.play(&his)
        })
        .collect::<Vec<usize>>()
        .iter()
        .sum()
}

fn main() {
    let output = full_game(INPUT);
    println!("{output}");
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../resources/test_input.txt");
    #[test]
    fn output() {
        assert_eq!(12, full_game(TEST_INPUT));
    }
}
