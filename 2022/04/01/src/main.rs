use itertools::Itertools;
use std::collections::HashSet;

const INPUT: &str = include_str!("../resources/input.txt");

fn number_of_subsets(string: &str) -> usize {
    string
        .trim()
        .split('\n')
        .into_iter()
        .map(|string| {
            string
                .split(',')
                .map(|range| {
                    range
                        .split('-')
                        .into_iter()
                        .map(|num| num.parse::<usize>().unwrap())
                        .collect::<Vec<usize>>()
                        .into_iter()
                        .tuples()
                        .map(|(one, two)| HashSet::from_iter(one..=two))
                        .next()
                        .unwrap()
                })
                .collect::<Vec<HashSet<usize>>>()
                .into_iter()
                .tuples()
                .map(|(one, two)| one.is_subset(&two) || two.is_subset(&one))
                .next()
                .unwrap()
        })
        .collect::<Vec<bool>>()
        .into_iter()
        .filter(|&b| b)
        .count()
}

fn main() {
    let total = number_of_subsets(INPUT);
    println!("{total}");
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../resources/test_input.txt");
    #[test]
    fn output() {
        assert_eq!(2, number_of_subsets(TEST_INPUT));
    }
}
