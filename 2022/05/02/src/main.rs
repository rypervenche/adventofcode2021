use itertools::Itertools;
use once_cell::sync::OnceCell;
use regex::Regex;

const INPUT: &str = include_str!("../resources/input.txt");

#[derive(Debug)]
struct Supplies {
    stacks: Vec<Stack>,
}

#[derive(Debug)]
struct Stack {
    crates: Vec<char>,
}

impl Supplies {
    fn change(&mut self, times: usize, from: usize, to: usize) {
        let mut tmp = Vec::new();
        for _ in 0..times {
            let thing = self.stacks[from - 1]
                .crates
                .pop()
                .expect("Stack is empty for change");
            tmp.push(thing);
        }
        tmp.reverse();
        self.stacks[to - 1].crates.extend(tmp);
    }

    fn top_crates(&self) -> String {
        let mut string = String::new();
        for stack in &self.stacks {
            let last = stack.crates.last().expect("Stack is empty for top crates");
            string.push(*last);
        }
        string
    }

    fn new(vec: Vec<Stack>) -> Self {
        Supplies { stacks: vec }
    }
}

impl Stack {
    fn new(vec: Vec<char>) -> Self {
        Stack { crates: vec }
    }
}

fn split_input(string: &str) -> (&str, &str) {
    string.trim().split("\n\n").next_tuple().unwrap()
}

fn parse_instructions(string: &str) -> Vec<(usize, usize, usize)> {
    let re: OnceCell<Regex> = OnceCell::new();
    let regex = re.get_or_init(|| {
        Regex::new(r"move (?P<times>\d+) from (?P<from>\d+) to (?P<to>\d+)").unwrap()
    });
    let mut vec = Vec::new();
    for line in string.lines() {
        let caps = regex.captures(line).unwrap();
        let times = caps
            .name("times")
            .map(|m| m.as_str().parse::<usize>().unwrap())
            .unwrap();
        let from = caps
            .name("from")
            .map(|m| m.as_str().parse::<usize>().unwrap())
            .unwrap();
        let to = caps
            .name("to")
            .map(|m| m.as_str().parse::<usize>().unwrap())
            .unwrap();
        vec.push((times, from, to));
    }
    vec
}

fn main() {
    let one = Stack::new(vec!['W', 'R', 'F']);
    let two = Stack::new(vec!['T', 'H', 'M', 'C', 'D', 'V', 'W', 'P']);
    let three = Stack::new(vec!['P', 'M', 'Z', 'N', 'L']);
    let four = Stack::new(vec!['J', 'C', 'H', 'R']);
    let five = Stack::new(vec!['C', 'P', 'G', 'H', 'Q', 'T', 'B']);
    let six = Stack::new(vec!['G', 'C', 'W', 'L', 'F', 'Z']);
    let seven = Stack::new(vec!['W', 'V', 'L', 'Q', 'Z', 'J', 'G', 'C']);
    let eight = Stack::new(vec!['P', 'N', 'R', 'F', 'W', 'T', 'V', 'C']);
    let nine = Stack::new(vec!['J', 'W', 'H', 'G', 'R', 'S', 'V']);
    let (supplies_ascii, movements) = split_input(INPUT);
    // let mut supplies = Supplies::new(supplies_ascii);
    let mut supplies = Supplies::new(vec![one, two, three, four, five, six, seven, eight, nine]);
    for (from, to, times) in parse_instructions(movements) {
        supplies.change(from, to, times);
    }
    let answer = supplies.top_crates();
    println!("{answer}");
    println!("CNSCZWLVT");
    // Answer: CNSCZWLVT
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = include_str!("../resources/test_input.txt");
    #[test]
    fn output() {
        let (supplies_ascii, movements) = split_input(TEST_INPUT);
        // let mut supplies = Supplies::new(supplies_ascii);
        let one = Stack::new(vec!['Z', 'N']);
        let two = Stack::new(vec!['M', 'C', 'D']);
        let three = Stack::new(vec!['P']);
        let mut supplies = Supplies::new(vec![one, two, three]);
        for (from, to, times) in parse_instructions(movements) {
            supplies.change(from, to, times);
        }
        let answer = supplies.top_crates();
        assert_eq!("MCD", answer);
    }
}
