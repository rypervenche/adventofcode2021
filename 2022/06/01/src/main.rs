use anyhow::{bail, Result};
use itertools::Itertools;

const INPUT: &str = include_str!("../resources/input.txt");
const LEN: usize = 4;

fn run(string: &str) -> Result<usize> {
    let len = string.len();
    for i in 0..len {
        let slice = &string[i..i + LEN];
        if slice.chars().unique().count() == LEN {
            return Ok(i + LEN);
        }
    }
    bail!("No consecutive unique digits exist.");
}

fn main() {
    let answer = run(INPUT.trim()).unwrap();
    println!("{answer}");
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn output() {
        let test1 = "bvwbjplbgvbhsrlpgdmjqwftvncz";
        let test2 = "nppdvjthqldpwncqszvftbrmjlhg";
        let test3 = "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg";
        let test4 = "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw";

        assert_eq!(5, run(test1).unwrap());
        assert_eq!(6, run(test2).unwrap());
        assert_eq!(10, run(test3).unwrap());
        assert_eq!(11, run(test4).unwrap());
    }
}
