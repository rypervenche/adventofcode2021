use anyhow::{bail, Result};
use itertools::Itertools;

const INPUT: &str = include_str!("../resources/input.txt");
const LEN: usize = 14;

fn run(string: &str) -> Result<usize> {
    let len = string.len();
    for i in 0..len {
        let slice = &string[i..i + LEN];
        if slice.chars().unique().count() == LEN {
            return Ok(i + LEN);
        }
    }
    bail!("No consecutive unique digits exist.");
}

fn main() {
    let answer = run(INPUT.trim()).unwrap();
    println!("{answer}");
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn output() {
        let test0 = "mjqjpqmgbljsphdztnvjfqwrcgsmlb";
        let test1 = "bvwbjplbgvbhsrlpgdmjqwftvncz";
        let test2 = "nppdvjthqldpwncqszvftbrmjlhg";
        let test3 = "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg";
        let test4 = "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw";

        assert_eq!(19, run(test0).unwrap());
        assert_eq!(23, run(test1).unwrap());
        assert_eq!(23, run(test2).unwrap());
        assert_eq!(29, run(test3).unwrap());
        assert_eq!(26, run(test4).unwrap());
    }
}
