const INPUT: &str = include_str!("../resources/input");

struct Counter {
    paren: usize,
    square: usize,
    curly: usize,
    angle: usize,
}

impl Counter {
    fn new() -> Counter {
        Counter {
            paren: 0,
            square: 0,
            curly: 0,
            angle: 0,
        }
    }

    fn count(&mut self, c: char) -> Option<char> {
        println!("c: {:#?}", c);
        match c {
            '(' => self.paren += 1,
            '[' => self.square += 1,
            '{' => self.curly += 1,
            '<' => self.angle += 1,

            ')' => {
                println!("paren: {}", self.paren);
                let (answer, overflow) = self.paren.overflowing_sub(1);
                if overflow {
                    return Some(c);
                } else {
                    self.paren = answer;
                }
                println!("paren: {}", self.paren);
            }
            ']' => {
                println!("square: {}", self.square);
                let (answer, overflow) = self.square.overflowing_sub(1);
                if overflow {
                    return Some(c);
                } else {
                    self.square = answer;
                }
                println!("square: {}", self.square);
            }
            '}' => {
                println!("curly: {}", self.curly);
                let (answer, overflow) = self.curly.overflowing_sub(1);
                if overflow {
                    return Some(c);
                } else {
                    self.curly = answer;
                }
                println!("curly: {}", self.curly);
            }
            '>' => {
                println!("angle: {}", self.angle);
                let (answer, overflow) = self.angle.overflowing_sub(1);
                if overflow {
                    return Some(c);
                } else {
                    self.angle = answer;
                }
                println!("angle: {}", self.angle);
            }
            _ => panic!("Wrong char"),
        }
        None
    }

    fn run_line(&mut self, line: &str) -> Option<char> {
        println!("new line");
        let results: Option<String> = line
            .chars()
            .map(|c| self.count(c))
            .filter(|&o| o.is_some())
            .take(1)
            .collect();

        println!("results: {:#?}", results);
        //*self = Counter::new();

        match results {
            // if line is good
            Some(s) if s == "" => None,
            Some(s) => Some(s.parse().unwrap()),
            None => None,
        }
    }
}

fn score(symbols: &[char]) -> usize {
    println!("symbols: {:#?}", symbols);
    symbols
        .iter()
        .map(|c| match c {
            ')' => 3,
            ']' => 57,
            '}' => 1197,
            '>' => 25137,
            _ => panic!("Bad input"),
        })
        .sum()
}

fn main() {
    //let mut counter = Counter::new();
    let answer: Vec<char> = INPUT
        .lines()
        .map(|l| {
            let mut counter = Counter::new();
            counter.run_line(l)
        })
        .filter(|o| o.is_some())
        .map(|c| c.unwrap())
        .collect();
    println!("answer: {}", score(&answer));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn good_input() {
        let input = "[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]";

        //let mut counter = Counter::new();
        //let answer: Vec<Option<char>> = input.lines().map(|l| counter.run_line(l)).collect();
        let answer: Vec<Option<char>> = input
            .lines()
            .map(|l| {
                let mut counter = Counter::new();
                counter.run_line(l)
            })
            .collect();
        //        let answer: Vec<char> = input
        //            .lines()
        //            .map(|l| counter.run_line(l))
        //            .collect::<Vec<Option<char>>>()
        //            .iter()
        //            .filter(|o| o.is_some())
        //            .map(|o| o.unwrap())
        //            .collect();
        //println!("answer: {}", score(&answer));
        println!("answer: {:#?}", answer);
        //assert_eq!(26397, score(&answer));
    }
}
