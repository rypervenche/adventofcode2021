const INPUT: &str = include_str!("../resources/input");

fn main() {
    let mut last = u32::MAX;
    let mut count = 0;
    let mut vec = Vec::new();
    for line in INPUT.lines() {
        let num: u32 = line.parse().unwrap();
        vec.push(num);
        if vec.len() == 3 {
            let sum = vec.iter().sum();
            if sum > last {
                count += 1;
            }
            last = sum;
            vec = vec[1..].to_owned();
        }
    }

    println!("Count: {}", count);
}
