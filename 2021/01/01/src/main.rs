const INPUT: &str = include_str!("../resources/input");

fn main() {
    let mut last = u32::MAX;
    let mut count = 0;
    for line in INPUT.lines() {
        let num: u32 = line.parse().unwrap();
        if num > last {
            count += 1;
        }
        last = num;
    }

    println!("Count: {}", count);
}
