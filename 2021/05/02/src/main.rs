use std::str::FromStr;

const INPUT: &str = include_str!("../resources/input");

#[derive(Debug, Clone, Copy)]
struct Line {
    x1: usize,
    y1: usize,
    x2: usize,
    y2: usize,
}

impl Line {
    fn new(vec: &[usize]) -> Self {
        Line {
            x1: vec[0],
            x2: vec[2],
            y1: vec[1],
            y2: vec[3],
        }
    }

    fn max(&self) -> usize {
        self.into_iter().max().unwrap() + 1
    }
}

struct LineIntoIter {
    line: Line,
    index: usize,
}

struct LineIter<'a> {
    line: &'a Line,
    index: usize,
}

impl IntoIterator for Line {
    type Item = usize;
    type IntoIter = LineIntoIter;
    fn into_iter(self) -> Self::IntoIter {
        LineIntoIter {
            line: self,
            index: 0,
        }
    }
}

impl<'a> IntoIterator for &'a Line {
    type Item = usize;
    type IntoIter = LineIter<'a>;
    fn into_iter(self) -> Self::IntoIter {
        LineIter {
            line: self,
            index: 0,
        }
    }
}

impl Iterator for LineIntoIter {
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item> {
        let current_item = match self.index {
            0 => Some(self.line.x1),
            1 => Some(self.line.y1),
            2 => Some(self.line.x2),
            3 => Some(self.line.y2),
            _ => return None,
        };

        self.index += 1;
        current_item
    }
}

impl<'a> Iterator for LineIter<'a> {
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item> {
        let current_item = match self.index {
            0 => Some(self.line.x1),
            1 => Some(self.line.y1),
            2 => Some(self.line.x2),
            3 => Some(self.line.y2),
            _ => return None,
        };

        self.index += 1;
        current_item
    }
}

impl FromStr for Line {
    type Err = ();

    fn from_str(input: &str) -> Result<Line, Self::Err> {
        let points: Vec<&str> = input.split(" -> ").collect();
        let vec2: Vec<usize> = points
            .iter()
            .map(|s| {
                s.split(',')
                    .collect::<Vec<&str>>()
                    .iter()
                    .map(|n| n.parse::<usize>().unwrap())
                    .collect::<Vec<_>>()
            })
            .flatten()
            .collect();
        Ok(Line::new(&vec2))
    }
}

fn to_map(map: &mut Vec<Vec<usize>>, line: &Line) {
    let mut x = line.x1;
    let mut y = line.y1;
    if line.x1 == line.x2 {
        // Vertical
        if y < line.y2 {
            while y <= line.y2 {
                if let Some(outer) = map.get_mut(y) {
                    if let Some(inner) = outer.get_mut(x) {
                        *inner += 1;
                    }
                }
                y += 1;
            }
        } else {
            while y >= line.y2 {
                if let Some(outer) = map.get_mut(y) {
                    if let Some(inner) = outer.get_mut(x) {
                        *inner += 1;
                    }
                }
                y -= 1;
            }
        }
        // Horizonal
    } else if line.y1 == line.y2 {
        if x < line.x2 {
            while x <= line.x2 {
                if let Some(outer) = map.get_mut(y) {
                    if let Some(inner) = outer.get_mut(x) {
                        *inner += 1;
                    }
                }
                x += 1;
            }
        } else {
            while x >= line.x2 {
                if let Some(outer) = map.get_mut(y) {
                    if let Some(inner) = outer.get_mut(x) {
                        *inner += 1;
                    }
                }
                x -= 1;
            }
        }
        // Diagonal
    } else if line.x1 != line.x2 && line.y1 != line.y2 {
        if x < line.x2 && y < line.y2 {
            while x < line.x2 && y < line.y2 {
                if let Some(outer) = map.get_mut(y) {
                    if let Some(inner) = outer.get_mut(x) {
                        *inner += 1;
                    }
                }
                x += 1;
                y += 1;
            }
        } else if x < line.x2 && y > line.y2 {
            while x < line.x2 && y > line.y2 {
                if let Some(outer) = map.get_mut(y) {
                    if let Some(inner) = outer.get_mut(x) {
                        *inner += 1;
                    }
                }
                x += 1;
                y -= 1;
            }
        }
        if x > line.x2 && y < line.y2 {
            while x > line.x2 && y < line.y2 {
                if let Some(outer) = map.get_mut(y) {
                    if let Some(inner) = outer.get_mut(x) {
                        *inner += 1;
                    }
                }
                x -= 1;
                y += 1;
            }
        } else if x > line.x2 && y > line.y2 {
            while x > line.x2 && y > line.y2 {
                if let Some(outer) = map.get_mut(y) {
                    if let Some(inner) = outer.get_mut(x) {
                        *inner += 1;
                    }
                }
                x -= 1;
                y -= 1;
            }
        } else {
            panic!("Something slipped through the if statement");
        }
        // Last
        if let Some(outer) = map.get_mut(y) {
            if let Some(inner) = outer.get_mut(x) {
                *inner += 1;
            }
        }
    }
}

fn calculate(map: &Vec<Vec<usize>>) -> usize {
    map.iter().flatten().filter(|n| **n >= 2).count()
}

fn main() {
    let mut max = 0;
    let mut lines = Vec::new();
    for string in INPUT.lines() {
        let line: Line = string.parse().unwrap();
        lines.push(line);
        let current_max = line.max();
        if max < current_max {
            max = current_max;
        }
    }
    let mut map = vec![vec![0; max]; max];
    for line in lines {
        to_map(&mut map, &line);
    }
    let answer = calculate(&map);
    println!("answer: {}", answer);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn intoiter() {
        let line = Line::new(&[1, 2, 3, 4]);
        let mut iter = line.into_iter();
        assert_eq!(Some(1), iter.next());
        assert_eq!(Some(2), iter.next());
        assert_eq!(Some(3), iter.next());
        assert_eq!(Some(4), iter.next());
        assert_eq!(None, iter.next());
    }
    #[test]
    fn iter() {
        let line = Line::new(&[1, 2, 3, 4]);
        let iter = &mut line.into_iter();
        assert_eq!(Some(1), iter.next());
        assert_eq!(Some(2), iter.next());
        assert_eq!(Some(3), iter.next());
        assert_eq!(Some(4), iter.next());
        assert_eq!(None, iter.next());
    }

    #[test]
    fn good_input() {
        let input = "0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2";
        let mut max = 0;
        let mut lines = Vec::new();
        for string in input.lines() {
            let line: Line = string.parse().unwrap();
            lines.push(line);
            let current_max = line.max();
            if max < current_max {
                max = current_max;
            }
        }
        let mut map = vec![vec![0; max]; max];
        for line in lines {
            to_map(&mut map, &line);
        }
        let answer = calculate(&map);
        assert_eq!(12, answer);
    }
}
