const INPUT: &str = include_str!("../resources/input");

struct HeightMap {
    map: Vec<Vec<u8>>,
}

impl HeightMap {
    fn new(input: &str) -> Self {
        let vec = parse_input(input);
        HeightMap { map: vec }
    }
    fn lowest_points(&self) -> Vec<u8> {
        let mut total = Vec::new();
        for (i, inner) in self.map.iter().enumerate() {
            for (j, elem) in inner.iter().enumerate() {
                if check_around(self, i, j) {
                    total.push(*elem);
                }
            }
        }
        total
    }
    fn lowest_sum(&self) -> usize {
        self.lowest_points().iter().map(|n| *n as usize + 1).sum()
    }
}

fn check_around(map: &HeightMap, i: usize, j: usize) -> bool {
    let i_len = map.map.len();
    let j_len = map.map[0].len();
    let mut bools = Vec::new();
    if i < i_len - 1 {
        if map.map[i + 1][j] > map.map[i][j] {
            bools.push(true);
        } else {
            bools.push(false);
        }
    }
    if i > 0 {
        if map.map[i - 1][j] > map.map[i][j] {
            bools.push(true);
        } else {
            bools.push(false);
        }
    }
    if j < j_len - 1 {
        if map.map[i][j + 1] > map.map[i][j] {
            bools.push(true);
        } else {
            bools.push(false);
        }
    }
    if j > 0 {
        if map.map[i][j - 1] > map.map[i][j] {
            bools.push(true);
        } else {
            bools.push(false);
        }
    }
    bools.iter().all(|&b| b)
}

fn parse_input(input: &str) -> Vec<Vec<u8>> {
    input
        .lines()
        .map(|l| {
            l.trim()
                .chars()
                .map(|c| c.to_digit(10).unwrap() as u8)
                .collect()
        })
        .collect()
}

fn main() {
    let map = HeightMap::new(INPUT);
    let answer = map.lowest_sum();
    println!("{}", answer);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn good_input() {
        let input = "2199943210
3987894921
9856789892
8767896789
9899965678";
        let map = HeightMap::new(input);
        let answer = map.lowest_sum();
        assert_eq!(15, answer);
    }
}
