use std::collections::HashSet;

const INPUT: &str = include_str!("../resources/input");

struct HeightMap {
    map: Vec<Vec<u8>>,
}

impl HeightMap {
    fn new(input: &str) -> Self {
        let vec = parse_input(input);
        HeightMap { map: vec }
    }
    fn basin_sizes(&self) -> Vec<HashSet<(usize, usize)>> {
        let mut total = Vec::new();
        for (i, inner) in self.map.iter().enumerate() {
            for (j, _elem) in inner.iter().enumerate() {
                if check_around(self, i, j) {
                    total.push(count_basin_points(self, i, j));
                }
            }
        }
        total
    }
}

fn count_basin_points(map: &HeightMap, i: usize, j: usize) -> HashSet<(usize, usize)> {
    let i_len = map.map.len();
    let j_len = map.map[0].len();
    let mut coords = HashSet::new();
    coords.insert((i, j));
    let current = map.map[i][j];

    if i < i_len - 1 && map.map[i + 1][j] > current && map.map[i + 1][j] < 9 {
        let lower = count_basin_points(map, i + 1, j);
        for tuple in lower {
            coords.insert(tuple);
        }
    }
    if i > 0 && map.map[i - 1][j] > current && map.map[i - 1][j] < 9 {
        let lower = count_basin_points(map, i - 1, j);
        for tuple in lower {
            coords.insert(tuple);
        }
    }
    if j < j_len - 1 && map.map[i][j + 1] > current && map.map[i][j + 1] < 9 {
        let lower = count_basin_points(map, i, j + 1);
        for tuple in lower {
            coords.insert(tuple);
        }
    }
    if j > 0 && map.map[i][j - 1] > current && map.map[i][j - 1] < 9 {
        let lower = count_basin_points(map, i, j - 1);
        for tuple in lower {
            coords.insert(tuple);
        }
    }
    coords
}

fn check_around(map: &HeightMap, i: usize, j: usize) -> bool {
    let i_len = map.map.len();
    let j_len = map.map[0].len();
    let mut bools = Vec::new();
    if i < i_len - 1 {
        if map.map[i + 1][j] > map.map[i][j] {
            bools.push(true);
        } else {
            bools.push(false);
        }
    }
    if i > 0 {
        if map.map[i - 1][j] > map.map[i][j] {
            bools.push(true);
        } else {
            bools.push(false);
        }
    }
    if j < j_len - 1 {
        if map.map[i][j + 1] > map.map[i][j] {
            bools.push(true);
        } else {
            bools.push(false);
        }
    }
    if j > 0 {
        if map.map[i][j - 1] > map.map[i][j] {
            bools.push(true);
        } else {
            bools.push(false);
        }
    }
    bools.iter().all(|&b| b)
}

fn parse_input(input: &str) -> Vec<Vec<u8>> {
    input
        .lines()
        .map(|l| {
            l.trim()
                .chars()
                .map(|c| c.to_digit(10).unwrap() as u8)
                .collect()
        })
        .collect()
}

fn main() {
    let map = HeightMap::new(INPUT);
    let coords = map.basin_sizes();
    let mut answers: Vec<usize> = coords.iter().map(|h| h.iter().count()).collect();

    answers.sort_unstable();
    answers.reverse();
    let three: usize = answers.iter().take(3).product();
    println!("{}", three);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn good_input() {
        let good_vec = vec![3, 9, 14, 9];
        let input = "2199943210
3987894921
9856789892
8767896789
9899965678";
        let map = HeightMap::new(input);
        let coords = map.basin_sizes();
        let mut answers: Vec<usize> = coords.iter().map(|h| h.iter().count()).collect();

        assert_eq!(good_vec, answers);

        answers.sort();
        answers.reverse();
        let three: usize = answers.iter().take(3).product();

        assert_eq!(1134, three);
    }
}
