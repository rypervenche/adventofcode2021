use std::collections::BTreeMap;

const INPUT: &str = include_str!("../resources/input");

fn parse_input(input: &str) -> Vec<usize> {
    input
        .split(',')
        .map(|s| s.trim().parse::<usize>().unwrap())
        .collect()
}

fn find_mean(nums: &[usize]) -> usize {
    let mut map = BTreeMap::new();
    for (i, num) in nums.iter().enumerate() {
        map.insert(i, num);
    }

    let max_height = map.values().max().unwrap();

    let mut closest = Vec::new();
    for height in 0..=**max_height {
        let abs = map
            .values()
            .map(|n| (**n as isize - height as isize).abs() as usize)
            .collect::<Vec<usize>>();
        let sum: usize = abs.iter().map(|n| triangle(*n)).sum();
        closest.push(sum);
    }

    let min = closest.iter().min().unwrap();
    println!("map: {:#?}", map);
    println!("closest: {:#?}", closest);
    *min
}

fn triangle(n: usize) -> usize {
    (n.pow(2) + n) / 2
}

fn main() {
    let input = parse_input(INPUT);
    let answer = find_mean(&input);
    println!("answer: {}", answer);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn good_input() {
        let input = "16,1,2,0,4,2,7,1,2,14";
        let input = parse_input(input);
        let answer = find_mean(&input);
        assert_eq!(168, answer);
    }
}
