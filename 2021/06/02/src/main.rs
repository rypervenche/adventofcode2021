const INPUT: &str = include_str!("../resources/input");

fn parse_input(input: &str) -> Vec<usize> {
    input
        .split(',')
        .map(|s| s.trim().parse::<usize>().unwrap())
        .collect()
}

fn convert_input(fishes: &mut Vec<usize>) -> Vec<usize> {
    let mut vec = vec![0; 9];
    for elem in fishes {
        if let Some(f) = vec.get_mut(*elem) {
            *f += 1;
        }
    }
    vec
}

fn pass_day(fishes: &mut Vec<usize>) {
    fishes.rotate_left(1);
    let new = fishes[8];
    fishes[6] += new;
}

fn main() {
    let mut input = parse_input(INPUT);
    let mut fishes = convert_input(&mut input);
    for d in 0..256 {
        pass_day(&mut fishes);
        let sum: usize = fishes.iter().sum();
        println!("d: {}, fishes: {}", d, sum);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn good_input() {
        let input = "3,4,3,1,2";
        let good = 26984457539;
        let mut input = parse_input(input);
        let mut fishes = convert_input(&mut input);
        for d in 0..256 {
            pass_day(&mut fishes);
            println!("d: {}, fishes: {:#?}", d, fishes);
        }
        let sum: usize = fishes.iter().sum();

        assert_eq!(good, sum);
    }
}
