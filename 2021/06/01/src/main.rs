const INPUT: &str = include_str!("../resources/input");

#[derive(Copy, Clone, Debug)]
struct Fish {
    counter: usize,
}

impl Fish {
    fn new(num: usize) -> Self {
        Self { counter: num }
    }

    fn age(&mut self) -> Option<Fish> {
        match self.counter {
            1..=8 => self.counter -= 1,
            0 => {
                self.counter = 6;
                return Some(Fish::new(8));
            }
            _ => panic!("Bad number for fish."),
        }
        None
    }
}

fn parse_input(input: &str) -> Vec<Fish> {
    input
        .split(',')
        .map(|s| Fish::new(s.trim().parse::<usize>().unwrap()))
        .collect()
}

fn pass_day(fishes: &mut Vec<Fish>) {
    let mut new_fishes = Vec::new();
    for fish in fishes.iter_mut() {
        if let Some(new) = fish.age() {
            new_fishes.push(new);
        }
    }
    fishes.extend(new_fishes);
}

fn main() {
    let mut fishes = parse_input(INPUT);
    for d in 0..80 {
        pass_day(&mut fishes);
        println!("d: {}, fishes: {}", d, fishes.len());
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn good_input() {
        let input = "3,4,3,1,2";
        let good = 5934;

        let mut fishes = parse_input(input);
        for _ in 0..80 {
            pass_day(&mut fishes);
        }
        assert_eq!(good, fishes.len());
    }
}
