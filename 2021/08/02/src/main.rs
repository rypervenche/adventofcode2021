use std::collections::{HashMap, HashSet};

const INPUT: &str = include_str!("../resources/input");

fn parse_digits(known: &mut HashMap<u8, HashSet<char>>, input: Vec<char>) {
    let input = HashSet::from_iter(input);
    match input.len() {
        2 => update_known(known, input, 1),
        3 => update_known(known, input, 7),
        4 => update_known(known, input, 4),
        7 => update_known(known, input, 8),
        _ => {}
    }
}

fn crack(known: &mut HashMap<u8, HashSet<char>>, digit: Vec<char>) {
    let digit = HashSet::from_iter(digit);
    for value in known.values() {
        if *value == digit {
            return;
        }
    }

    if let Some(four) = known.get(&4) {
        if let Some(seven) = known.get(&7) {
            let four_seven = four.union(&seven).cloned().collect::<HashSet<_>>();
            if digit.is_superset(&four_seven) {
                update_known(known, digit.clone(), 9);
                return;
            }
        }
    }
    if let Some(seven) = known.get(&7) {
        if let Some(nine) = known.get(&9) {
            if &digit.union(&seven).cloned().collect::<HashSet<_>>() == nine {
                update_known(known, digit.clone(), 5);
                return;
            }
        }
    }
    if let Some(seven) = known.get(&7) {
        if let Some(eight) = known.get(&8) {
            if &digit.union(&seven).cloned().collect::<HashSet<_>>() == eight {
                update_known(known, digit.clone(), 6);
                return;
            }
        }
    }
    if let Some(four) = known.get(&4) {
        if let Some(nine) = known.get(&9) {
            let union = four.union(&digit).cloned().collect::<HashSet<_>>();
            if union == *nine {
                update_known(known, digit.clone(), 3);
                return;
            }
        }
    }
    if known.len() == 8 || known.len() == 9 {
        if digit.len() == 5 {
            update_known(known, digit.clone(), 2);
        } else if digit.len() == 6 {
            update_known(known, digit.clone(), 0);
        }
    }
}

fn update_known(known: &mut HashMap<u8, HashSet<char>>, input: HashSet<char>, num: u8) {
    let set = HashSet::from_iter(input);
    known.insert(num, set);
}

fn parse_line(input: &str) -> (Vec<Vec<char>>, Vec<Vec<char>>) {
    let mut split = input.split(" | ");
    let patterns: Vec<Vec<char>> = split
        .next()
        .unwrap()
        .split(' ')
        .map(|s| s.chars().collect::<Vec<char>>())
        .collect();

    let digits: Vec<Vec<char>> = split
        .next()
        .unwrap()
        .split(' ')
        .map(|s| s.chars().to_owned().collect::<Vec<char>>())
        .collect();
    (patterns, digits)
}

fn main() {
    let mut answers = Vec::new();
    for line in INPUT.lines() {
        let mut known: HashMap<u8, HashSet<char>> = HashMap::new();
        let (patterns, digits) = parse_line(line);
        for pattern in &patterns {
            parse_digits(&mut known, pattern.to_vec());
        }
        while known.len() < 10 {
            for pattern in &patterns {
                crack(&mut known, pattern.to_vec());
            }
        }
        let mut answer = Vec::new();
        for digit in digits {
            let hash = HashSet::from_iter(digit);
            for (k, v) in &known {
                if hash == *v {
                    answer.push(*k);
                }
            }
        }
        answers.push(to_number(answer));
    }
    let answer: usize = answers.into_iter().sum();
    println!("{:#?}", answer);
}

fn to_number(vec: Vec<u8>) -> usize {
    vec.iter()
        .map(|n| n.to_string())
        .collect::<String>()
        .parse()
        .unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn good_input_single() {
        let mut known: HashMap<u8, HashSet<char>> = HashMap::new();
        let input =
            "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf";
        let (patterns, digits) = parse_line(input);
        for pattern in &patterns {
            parse_digits(&mut known, pattern.to_vec());
        }
        while known.len() < 10 {
            for pattern in &patterns {
                crack(&mut known, pattern.to_vec());
            }
        }
        let mut answer = Vec::new();
        for digit in digits {
            let hash = HashSet::from_iter(digit);
            for (k, v) in &known {
                if hash == *v {
                    answer.push(*k);
                }
            }
        }
        assert_eq!(5353, to_number(answer));
    }

    #[test]
    fn good_input_full() {
        let input =
            "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce";
        let mut answers = Vec::new();
        for line in input.lines() {
            let mut known: HashMap<u8, HashSet<char>> = HashMap::new();
            let (patterns, digits) = parse_line(line);
            for pattern in &patterns {
                parse_digits(&mut known, pattern.to_vec());
            }
            while known.len() < 10 {
                for pattern in &patterns {
                    crack(&mut known, pattern.to_vec());
                }
            }
            let mut answer = Vec::new();
            for digit in digits {
                let hash = HashSet::from_iter(digit);
                for (k, v) in &known {
                    if hash == *v {
                        answer.push(*k);
                    }
                }
            }
            answers.push(to_number(answer));
        }
        println!("{:#?}", answers);
        let answer: usize = answers.into_iter().sum();
        assert_eq!(61229, answer);
    }
}
