const INPUT: &str = include_str!("../resources/input");

#[derive(Debug)]
struct Digit {
    digit: Vec<char>,
}

impl Digit {
    fn new(n: Vec<char>) -> Self {
        Digit { digit: n }
    }

    fn is_unique(&self) -> bool {
        match self.digit.len() {
            2 | 3 | 4 | 7 => true,
            _ => false,
        }
    }
}

fn parse_input(input: &str) -> Vec<Digit> {
    let string: Vec<&str> = input
        .lines()
        .map(|l| l.split('|').nth(1).unwrap())
        .collect();

    let digits: Vec<Digit> = string
        .iter()
        .map(|s| s.trim().split(' ').map(|s| Digit::new(s.chars().collect())))
        .flatten()
        .collect();
    digits
}

fn main() {
    let input = parse_input(INPUT);
    let num = input.iter().filter(|d| d.is_unique()).count();
    println!("num: {}", num);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn good_input() {
        let input =
            "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce";
        let input = parse_input(input);
        let answer = input.iter().filter(|d| d.is_unique()).count();
        assert_eq!(26, answer);
    }
}
