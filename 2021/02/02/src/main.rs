use std::str::FromStr;

const INPUT: &str = include_str!("../resources/input");

struct Ship {
    horizontal: u32,
    depth: u32,
    aim: u32,
}

impl Ship {
    fn new() -> Self {
        Ship {
            horizontal: 0,
            depth: 0,
            aim: 0,
        }
    }

    fn distance(&self) -> u32 {
        self.horizontal * self.depth
    }

    fn movement(&mut self, direction: Direction, num: u32) {
        match direction {
            Direction::Forward => {
                self.horizontal += num;
                self.depth += self.aim * num;
            }
            Direction::Up => {
                if num < self.aim {
                    self.aim -= num;
                } else {
                    self.aim = 0;
                }
            }
            Direction::Down => self.aim += num,
        };
    }
}

enum Direction {
    Forward,
    Down,
    Up,
}

impl FromStr for Direction {
    type Err = ();

    fn from_str(input: &str) -> Result<Direction, Self::Err> {
        match input {
            "forward" => Ok(Direction::Forward),
            "down" => Ok(Direction::Down),
            "up" => Ok(Direction::Up),
            _ => panic!("Incorrect direction"),
        }
    }
}

fn main() {
    let mut ship = Ship::new();
    for line in INPUT.lines() {
        let mut split = line.split(' ');
        let direction: Direction = split.next().unwrap().parse().unwrap();
        let num: u32 = split.next().unwrap().parse().unwrap();

        ship.movement(direction, num);
    }

    println!("Distance: {}", ship.distance());
}
