use std::collections::HashMap;

const INPUT: &str = include_str!("../resources/input");

fn main() {
    let len = INPUT.lines().next().unwrap().len();
    let mut digits: Vec<HashMap<char, u32>> = vec![HashMap::new(); len];
    for binary in INPUT.lines() {
        for (i, c) in binary.chars().enumerate() {
            *digits[i].entry(c).or_insert(0) += 1;
        }
    }

    let gamma: String = digits
        .iter()
        .map(|hash| if hash[&'0'] > hash[&'1'] { '0' } else { '1' })
        .collect();

    let gamma = u16::from_str_radix(&gamma, 2).unwrap();
    let epsilon = !gamma & 0b0000111111111111u16;

    println!("Consumption power: {}", gamma as u32 * epsilon as u32);
}
