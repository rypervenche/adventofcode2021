use std::cmp::Ordering;
use std::collections::HashMap;

const INPUT: &str = include_str!("../resources/input");

fn run(report: Vec<&str>, index: usize, value: char) -> Vec<&str> {
    let mut digits: HashMap<char, u32> = HashMap::new();

    let _ = *digits.entry('0').or_insert(0);
    let _ = *digits.entry('1').or_insert(0);
    for binary in &report {
        let c = binary.chars().nth(index).unwrap();
        *digits.entry(c).or_insert(0) += 1;
    }

    let larger: char = if value == '1' {
        match digits[&'0'].cmp(&digits[&'1']) {
            Ordering::Less => '1',
            Ordering::Equal => value,
            Ordering::Greater => '0',
        }
    } else {
        match digits[&'0'].cmp(&digits[&'1']) {
            Ordering::Less => '0',
            Ordering::Equal => value,
            Ordering::Greater => '1',
        }
    };

    report
        .into_iter()
        .filter(|b| b.chars().nth(index).unwrap() == larger)
        .collect()
}

fn main() {
    let len = INPUT.lines().next().unwrap().len();
    let mut oxygen: Vec<&str> = INPUT.lines().collect();
    let mut co2: Vec<&str> = INPUT.lines().collect();
    for i in 0..len {
        if oxygen.len() == 1 {
            break;
        }
        oxygen = run(oxygen, i, '1');
    }

    for i in 0..len {
        if co2.len() == 1 {
            break;
        }
        co2 = run(co2, i, '0');
    }

    let oxygen = u32::from_str_radix(oxygen[0], 2).unwrap();
    let co2 = u32::from_str_radix(co2[0], 2).unwrap();
    println!("final: {:?}", oxygen);
    println!("final: {:?}", co2);
    println!("result: {}", oxygen * co2);
}
