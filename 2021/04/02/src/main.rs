use std::str::FromStr;

const INPUT: &str = include_str!("../resources/input");

#[derive(Debug, Clone)]
struct Bingo {
    board: Vec<Vec<u32>>,
    last: u32,
}

impl Bingo {
    fn play(&mut self, number: u32) {
        for row in self.board.iter_mut() {
            for elem in row {
                if *elem == number {
                    *elem = u32::MAX;
                    self.last = number;
                }
            }
        }
    }

    fn bingo(&self) -> bool {
        for row in &self.board {
            if row.iter().all(|n| *n == u32::MAX) {
                return true;
            }
        }
        let len = self.board.len();
        for i in 0..len {
            if self.board.iter().all(|r| r[i] == u32::MAX) {
                return true;
            }
        }
        false
    }

    fn calculate(&self) -> u32 {
        let mut sum = 0;
        for row in &self.board {
            for elem in row {
                if *elem != u32::MAX {
                    sum += *elem;
                }
            }
        }
        sum * self.last
    }
}

impl FromStr for Bingo {
    type Err = ();

    fn from_str(input: &str) -> Result<Bingo, Self::Err> {
        let vec: Vec<Vec<u32>> = input
            .lines()
            .map(|l| {
                l.split(' ')
                    .map(|n| n.trim().parse::<u32>().unwrap())
                    .collect::<Vec<u32>>()
            })
            .collect();
        Ok(Bingo {
            board: vec,
            last: u32::MAX,
        })
    }
}

fn find_bingo(boards: &mut Vec<Bingo>, numbers: &[u32]) -> usize {
    for num in numbers {
        for (i, board) in boards.iter_mut().enumerate() {
            board.play(*num);
            if board.bingo() {
                return i;
            }
        }
    }
    panic!("There should be a bingo here...");
}

fn main() {
    let numbers: Vec<u32> = INPUT
        .lines()
        .next()
        .unwrap()
        .split(',')
        .map(|n| n.parse::<u32>().unwrap())
        .collect();
    let inputs: String = INPUT.lines().skip(2).collect::<Vec<&str>>().join("\n");
    let inputs = inputs.replace("  ", " ").replace("\n ", "\n");
    let mut boards: Vec<Bingo> = inputs
        .split("\n\n")
        .map(|s| s.parse::<Bingo>().unwrap())
        .collect();

    while boards.len() > 1 {
        println!("len: {}", boards.len());
        let i = find_bingo(&mut boards, &numbers);
        boards.remove(i);
    }
    let _ = find_bingo(&mut boards, &numbers);
    println!("score: {:#?}", &boards[0]);
    println!("score: {}", &boards[0].calculate());
}
