use std::str::FromStr;

const INPUT: &str = include_str!("../resources/input");

#[derive(Debug)]
struct Bingo {
    board: Vec<Vec<u8>>,
    last: u8,
}

impl Bingo {
    fn play(&mut self, number: u8) {
        let mut count = 0;
        for row in self.board.iter_mut() {
            for elem in row {
                if *elem == number {
                    *elem = 0;
                    self.last = number;
                    count += 1;
                }
            }
        }
    }

    fn bingo(&self) -> bool {
        for row in &self.board {
            if row.iter().all(|n| *n == 0) {
                return true;
            }
        }
        let len = self.board.len();
        for i in 0..len {
            if self.board.iter().all(|r| r[i] == 0) {
                return true;
            }
        }
        false
    }

    fn calculate(&self) -> u32 {
        let mut sum = 0;
        for row in &self.board {
            for elem in row {
                if *elem != 0 {
                    sum += *elem as u32;
                }
            }
        }
        sum * self.last as u32
    }
}

impl FromStr for Bingo {
    type Err = ();

    fn from_str(input: &str) -> Result<Bingo, Self::Err> {
        let vec: Vec<Vec<u8>> = input
            .lines()
            .map(|l| {
                l.split(' ')
                    .map(|n| n.trim().parse::<u8>().unwrap())
                    .collect::<Vec<u8>>()
            })
            .collect();
        Ok(Bingo {
            board: vec,
            last: 0,
        })
    }
}

fn main() {
    let numbers: Vec<u8> = INPUT
        .lines()
        .next()
        .unwrap()
        .split(',')
        .map(|n| n.parse::<u8>().unwrap())
        .collect();
    let inputs: String = INPUT.lines().skip(2).collect::<Vec<&str>>().join("\n");
    let inputs = inputs.replace("  ", " ").replace("\n ", "\n");
    let mut boards: Vec<Bingo> = inputs
        .split("\n\n")
        .map(|s| s.parse::<Bingo>().unwrap())
        .collect();

    'outer: for num in numbers {
        for board in boards.iter_mut() {
            board.play(num);
            if board.bingo() {
                println!("score: {}", &board.calculate());
                break 'outer;
            }
        }
    }
}
